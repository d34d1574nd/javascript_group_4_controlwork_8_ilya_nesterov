import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://quoteproje.firebaseio.com/'
});

export default instance;